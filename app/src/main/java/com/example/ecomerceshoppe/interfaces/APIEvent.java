package com.example.ecomerceshoppe.interfaces;


import org.json.JSONObject;

public interface APIEvent {

    public void onSuccess(JSONObject response);

}
